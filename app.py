from flask import Flask, render_template, request, session, url_for, redirect
import MySQLdb
from random import randint

db = MySQLdb.connect(host='localhost', user='root', passwd='', db='railway_reservation')
db.autocommit(True)
cursor = db.cursor()
dictcursor = db.cursor(MySQLdb.cursors.DictCursor)
app = Flask(__name__)
app.config['SECRET_KEY'] = '1234556789'

@app.route('/',methods=['GET', 'POST'])
@app.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'GET':
        print(session.get('user'))
        if session.get('user'):
            print "entered if"
            return redirect(url_for('booking'))
        else:
            return render_template('login.html')
    if request.method == 'POST':
        email = request.form['InputEmail']
        password = request.form['InputPassword']
        sql = ''' select password from users where email = '{}';'''.format(email)
        row_count = cursor.execute(sql)
        if row_count == 0:
            return "invalid emali"
        if cursor.fetchone()[0] == password:
            session['user'] = email
            return redirect(url_for('booking'))
        return "Invalid Login"

@app.route('/register', methods=['GET','POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')
    if request.method == 'POST':
        print "entered post stage"
        email = request.form['InputEmail']
        password1 = request.form['InputPassword1']
        password2 = request.form['InputPassword2']
        name = request.form['InputName']
        dob = request.form['InputDob']
        address = request.form['InputAddress']
        phone = request.form['InputPhone']
        print "before chck"
        if password1 == password2:
            sql = ''' INSERT INTO users VALUES ('{}','{}','{}','{}','{}','{}'); '''.format(email,name,password1,dob,address,phone)
            print sql
            cursor.execute(sql)
            return redirect(url_for('login'))
        else:
            return "pass1 != pass2"
        return "Something went wrong"

@app.route('/booking', methods=['GET', 'POST'])
def booking():
    if request.method == 'GET':
        sql = '''select distinct station from train_intermediate'''
        cursor.execute(sql)
        stations = [x for y in cursor.fetchall() for x in y]
        return render_template('booking.html',stations=stations)
    if request.method == 'POST':
        source = request.form['InputSource']
        destination = request.form['InputDestination']
        if source == destination:
            return "Please select different source and destination"
        seats = request.form['InputSeats']
        doj = request.form['InputDoj']
        sql = '''select train_number from train_intermediate where station = '{}'; '''.format(source)
        dictcursor.execute(sql)
        train_no = dictcursor.fetchall()
        train_no = [x['train_number'] for x in train_no]
        if len(train_no) > 1:
            sql = '''select train_number from train_intermediate where train_number in '{}' and station = '{}';'''.format(tuple(train_no),destination)
        else:
            sql = '''select train_number from train_intermediate where train_number = '{}' and station = '{}';'''.format(train_no[0],destination)
        dictcursor.execute(sql)
        train_no = dictcursor.fetchall()
        train_no = [x['train_number'] for x in train_no]
        train_info = []
        for n,train in enumerate(train_no):
            train_info.append({})
            train_info[n]['train_number'] = train
            sql = '''select train_name from train where train_number = '{}';'''.format(train)
            cursor.execute(sql)
            train_info[n]['train_name'] = cursor.fetchone()[0]
            sql = '''select arrival from train_intermediate where train_number = '{}' and station = '{}';'''.format(train,source)
            cursor.execute(sql)
            train_info[n]['boarding_arrival'] = cursor.fetchone()[0]
            sql = '''select arrival from train_intermediate where train_number = '{}' and station = '{}';'''.format(train,destination)
            cursor.execute(sql)
            train_info[n]['destination_arrival'] = cursor.fetchone()[0]
            sql = '''select seats_available from availability where train_number = '{}' and doj = '{}';'''.format(train,doj)
            row_count = cursor.execute(sql)
            seats_available = cursor.fetchone()
            print seats_available
            if not row_count: 
                seats_available = '50'
                sql = '''insert into availability values ('{}','{}','{}');'''.format(train,doj,seats_available)
                cursor.execute(sql)
            else:
                seats_available = seats_available[0]
            train_info[n]['seats_available'] = seats_available
        return render_template('confirm_booking.html',train_info=train_info,source=source,destination=destination,seats=seats,doj=doj)

@app.route('/confirm_booking', methods=['GET','POST'])
def confirm_booking():
    if request.method == 'POST':
        source = request.args.get('source')        
        destination = request.args.get('destination')        
        doj = request.args.get('doj')        
        seats = int(request.args.get('seats'))        
        train = request.form.get('train')
        booking_number = ''.join(["%s" % randint(0, 9) for num in range(0, 6)])
        sql = '''select seats_available from availability where train_number='{}' and doj='{}';'''.format(train,doj) 
        cursor.execute(sql)
        seats_available = int(cursor.fetchone()[0])
        if seats <= seats_available:
            sql='''select booking_number from booking where train_number='{}' and doj='{}';'''.format(train,doj)
            row_count = cursor.execute(sql)
            bookings_on_that_train = cursor.fetchall()
            if row_count == 1:
                sql='''select seat_number from seat_allocated where booking_number = '{}';'''.format(bookings_on_that_train[0][0])
                cursor.execute(sql)
                reserved_seats = [int(x) for y in cursor.fetchall() for x in y]
                free_seats = [x for x in range(1,51) if x not in reserved_seats]
                #last_seat = int(cursor.fetchone()[0])
            elif row_count > 1:
                bookings_on_that_train =  tuple([x for y in bookings_on_that_train for x in y])
                sql='''select seat_number from seat_allocated where booking_number in {};'''.format(bookings_on_that_train)
                cursor.execute(sql)
                reserved_seats = [int(x) for y in cursor.fetchall() for x in y]
                free_seats = [x for x in range(1,51) if x not in reserved_seats]
                #last_seat = int(cursor.fetchone()[0])
            else:
                free_seats = list(range(1,51))
                last_seat = 0
            sql ='''update availability set seats_available='{}' where train_number ='{}' and doj='{}';'''.format(str(seats_available-seats),train,doj)
            cursor.execute(sql)
            sql='''insert into booking values ('{}','{}','{}','{}','{}','{}');'''.format(booking_number,session['user'],source,destination,doj,train)
            cursor.execute(sql)
            for x in range(seats):
                new_seat = free_seats.pop(0)
                sql='''insert into seat_allocated values ('{}','{}');'''.format(booking_number,str(new_seat))
                cursor.execute(sql)
            return redirect(url_for('booking'))
        else:
            seats_in_wl = seats - seats_available
            seats_to_be_reserved = seats - seats_in_wl
            sql='''select booking_number from booking where train_number='{}' and doj='{}';'''.format(train,doj)
            row_count = cursor.execute(sql)
            bookings_on_that_train = cursor.fetchall()
            if row_count == 1:
                sql='''select seat_number from seat_allocated where booking_number = '{}';'''.format(bookings_on_that_train[0][0])
                cursor.execute(sql)
                reserved_seats = [int(x) for y in cursor.fetchall() for x in y]
                free_seats = [x for x in range(1,51) if x not in reserved_seats]
                #last_seat = int(cursor.fetchone()[0])
            elif row_count > 1:
                bookings_on_that_train =  tuple([x for y in bookings_on_that_train for x in y])
                sql='''select seat_number from seat_allocated where booking_number in {};'''.format(bookings_on_that_train)
                cursor.execute(sql)
                reserved_seats = [int(x) for y in cursor.fetchall() for x in y]
                free_seats = [x for x in range(1,51) if x not in reserved_seats]
                #last_seat = int(cursor.fetchone()[0])
            else:
                free_seats = list(range(1,51))
                last_seat = 0
            sql='''select waiting_list_number from waiting_list where train_number='{}' and doj='{}';'''.format(train,doj)
            row_count = cursor.execute(sql)
            if not row_count:
                last_waiting_number = 0
            else:
                last_waiting_number = max([int(x) for y in cursor.fetchall() for x in y])
            sql ='''update availability set seats_available='{}' where train_number ='{}' and doj='{}';'''.format(0,train,doj)
            cursor.execute(sql)
            sql='''insert into booking values ('{}','{}','{}','{}','{}','{}');'''.format(booking_number,session['user'],source,destination,doj,train)
            cursor.execute(sql)
            if free_seats != []: 
                for x in range(seats_to_be_reserved):
                        new_seat = free_seats.pop(0)
                        sql='''insert into seat_allocated values ('{}','{}');'''.format(booking_number,str(new_seat))
                        cursor.execute(sql)
            for x in range(1,seats_in_wl+1):
                    sql='''insert into waiting_list values ('{}','{}','{}','{}');'''.format(booking_number,train,doj,str(last_waiting_number+x))
                    cursor.execute(sql)
            return redirect(url_for('booking'))
        return redirect(url_for('booking'))

@app.route('/cancellation',methods=['GET','POST'])
def cancellation():
    if request.method == 'GET':
        sql = '''select booking_number from booking where email = '{}';'''.format(session['user'])
        cursor.execute(sql)
        booking_number = [x for y in cursor.fetchall() for x in y]
        return render_template('cancellation.html',booking_number=booking_number)
    if request.method == 'POST':
        booking_number = request.form['BookingNumber']
        sql = '''select * from booking where booking_number = '{}';'''.format(booking_number)
        dictcursor.execute(sql)
        result = dictcursor.fetchone()
        doj = result['doj']
        train_number = result['train_number']
        sql = '''select waiting_list_number from waiting_list where booking_number = '{}';'''.format(booking_number)
        seats_in_wl = cursor.execute(sql)
        if seats_in_wl:
            seat_nos_in_wl = [int(x) for y in cursor.fetchall() for x in y]
        sql = '''select seat_number from seat_allocated where booking_number = '{}';'''.format(booking_number)
        seats_in_reservation = cursor.execute(sql)
        if seats_in_reservation:
            free_seat_numbers = [x for y in cursor.fetchall() for x in y]
        sql='''delete from booking where booking_number = '{}';'''.format(booking_number)
        cursor.execute(sql)
        sql='''delete from waiting_list where booking_number = '{}';'''.format(booking_number)
        cursor.execute(sql)
        sql='''delete from seat_allocated where booking_number = '{}';'''.format(booking_number)
        cursor.execute(sql)
        if seats_in_reservation:
            sql ='''select seats_available from availability where train_number = '{}' and doj = '{}';'''.format(train_number,doj)
            cursor.execute(sql)
            seats_available = int(cursor.fetchone()[0])
            sql = '''update availability set seats_available = '{}';'''.format(str(seats_available+seats_in_reservation))
            cursor.execute(sql)
        sql='''select * from waiting_list where train_number = '{}' and doj = '{}' and waiting_list_number <> '0';'''.format(train_number,doj)
        row_count = cursor.execute(sql)
        print row_count
        if row_count:
            waiting_list = cursor.fetchall()
            for wl in waiting_list:
                wl_no = max(0,int(wl[3])-seats_in_reservation)
                if seats_in_wl:
                    if int(wl[3]) > max(seat_nos_in_wl):
                        wl_no = max(0,int(wl[3]) - len(seat_nos_in_wl))
                if wl_no == 0:
                    print free_seat_numbers
                    new_seat_no = free_seat_numbers.pop()
                    sql = '''insert into seat_allocated values ('{}','{}');'''.format(wl[0],new_seat_no)
                    cursor.execute(sql)
                    sql = '''select seats_available from availability where train_number = '{}' and doj = '{}';'''.format(wl[1],wl[2])
                    cursor.execute(sql)
                    old_seats_available = int(cursor.fetchone()[0])
                    sql = '''update availability set seats_available = '{}' where train_number = '{}' and doj = '{}';'''.format(str(old_seats_available-1),wl[1],wl[2])
                    cursor.execute(sql)
                sql = '''update waiting_list set waiting_list_number = '{}' where train_number = '{}' and waiting_list_number = '{}' and doj = '{}';'''.format(wl_no,wl[1],wl[3],wl[2])
                cursor.execute(sql)
        return redirect(url_for('booking'))

@app.route('/status',methods=['GET','POST'])
def status():
    if request.method == 'GET':
        if not session.get('user'):
            return redirect('/login')
        sql = '''select booking_number from booking where email = '{}';'''.format(session.get('user'))
        row_count = cursor.execute(sql)
        if row_count == 0:
            return "You have no bookings"
        booking_number = [x for y in cursor.fetchall() for x in y]
        return render_template('status.html',booking_number=booking_number)
    if request.method == 'POST':
        booking_number = request.form['BookingNumber']
        sql = '''select seat_number from seat_allocated where booking_number = '{}';'''.format(booking_number)
        row_count = cursor.execute(sql)
        if row_count:
            seat_allocated = [x for y in cursor.fetchall() for x in y]
        else:
            seat_allocated = None
        sql = '''select waiting_list_number from waiting_list where booking_number = '{}' and waiting_list_number <> 0;'''.format(booking_number)
        row_count = cursor.execute(sql)
        if row_count:
            seat_in_waiting_list = [ x for y in cursor.fetchall() for x in y]
        else:
            seat_in_waiting_list = None 
        return render_template('status_confirm.html',seat_allocated=seat_allocated,seat_in_waiting_list=seat_in_waiting_list)



        
if __name__ == '__main__':
    app.run(debug=True)
