-- MySQL dump 10.16  Distrib 10.1.28-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: railway_reservation
-- ------------------------------------------------------
-- Server version	10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `availability`
--

DROP TABLE IF EXISTS `availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `availability` (
  `train_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doj` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seats_available` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `train_number` (`train_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `availability`
--

LOCK TABLES `availability` WRITE;
/*!40000 ALTER TABLE `availability` DISABLE KEYS */;
INSERT INTO `availability` VALUES ('16346','2','28');
/*!40000 ALTER TABLE `availability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking` (
  `booking_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boarding` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doj` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `train_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`booking_number`),
  KEY `train_number` (`train_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` VALUES ('091584','ak@as.co','Kannur','','2','16346'),('610805','ak@as.co','Kollam','','2','16346');
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seat_allocated`
--

DROP TABLE IF EXISTS `seat_allocated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seat_allocated` (
  `booking_number` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seat_number` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `booking_number` (`booking_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seat_allocated`
--

LOCK TABLES `seat_allocated` WRITE;
/*!40000 ALTER TABLE `seat_allocated` DISABLE KEYS */;
INSERT INTO `seat_allocated` VALUES ('610805','1'),('610805','2'),('610805','3'),('610805','4'),('610805','5'),('610805','6'),('610805','7'),('610805','8'),('610805','9'),('610805','10'),('610805','11'),('610805','12'),('610805','13'),('610805','14'),('610805','15'),('610805','16'),('610805','17'),('610805','18'),('610805','19'),('610805','20'),('610805','21'),('610805','22');
/*!40000 ALTER TABLE `seat_allocated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `train`
--

DROP TABLE IF EXISTS `train`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `train` (
  `train_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `train_number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destination` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`train_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `train`
--

LOCK TABLES `train` WRITE;
/*!40000 ALTER TABLE `train` DISABLE KEYS */;
INSERT INTO `train` VALUES ('Lucknow SF','12107','Lokmanya Tilak','Lucknow'),('Nizamuddin Darib Rath','12909','Bandra','Hazrat Nizamuddin'),('Netravati Express','16346','Trivandrum','Lokmanya Tilak ');
/*!40000 ALTER TABLE `train` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `train_intermediate`
--

DROP TABLE IF EXISTS `train_intermediate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `train_intermediate` (
  `train_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `arrival` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `train_number` (`train_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `train_intermediate`
--

LOCK TABLES `train_intermediate` WRITE;
/*!40000 ALTER TABLE `train_intermediate` DISABLE KEYS */;
INSERT INTO `train_intermediate` VALUES ('16346','Trivandrum','9:50'),('16346','Kollam','10:55'),('16346','Ernakulam','14:05'),('16346','Calicut','19:00'),('16346','Kannur','20:30'),('16346','Mangaluru','23:10'),('16346','Madgaon','5:50'),('16346','Ratnagiri','11:05'),('16346','Panvel','16:15'),('16346','Thane','17:01'),('16346','Lokmanya Tilak','17:45'),('12909','Bandra','16:35'),('12909','Borivali','17:08'),('12909','Surat','20:10'),('12909','Vadodara','21:46'),('12909','Dahod','23:32'),('12909','Ratlam','03:45'),('12909','Kota','03:45'),('12909','Mathura','07:27'),('12909','Hazrat Nizamudd','09:40'),('12107','Lokmanya Tilak','16:25'),('12107','Thane','16:45'),('12107','Bhusaval','22:30'),('12107','Bhopal','04:55'),('12107','Jhansi','09:10'),('12107','Kanpur','13:30'),('12107','Lucknow','15:10');
/*!40000 ALTER TABLE `train_intermediate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `email` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('ak@as.co','12','12','12','12','12'),('sh@sh.com','12','12','12','12','12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waiting`
--

DROP TABLE IF EXISTS `waiting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waiting` (
  `booking_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doj` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `train_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waiting`
--

LOCK TABLES `waiting` WRITE;
/*!40000 ALTER TABLE `waiting` DISABLE KEYS */;
INSERT INTO `waiting` VALUES ('091584','2','16346');
/*!40000 ALTER TABLE `waiting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waiting_list`
--

DROP TABLE IF EXISTS `waiting_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waiting_list` (
  `booking_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `train_number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doj` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `waiting_list_number` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  KEY `train_number` (`train_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waiting_list`
--

LOCK TABLES `waiting_list` WRITE;
/*!40000 ALTER TABLE `waiting_list` DISABLE KEYS */;
INSERT INTO `waiting_list` VALUES ('091584','16346','2','1'),('091584','16346','2','2'),('091584','16346','2','3'),('091584','16346','2','4'),('091584','16346','2','5'),('091584','16346','2','6'),('091584','16346','2','7'),('091584','16346','2','8'),('091584','16346','2','9'),('091584','16346','2','10'),('091584','16346','2','11'),('091584','16346','2','12'),('091584','16346','2','13'),('091584','16346','2','14'),('091584','16346','2','15'),('091584','16346','2','16'),('091584','16346','2','17'),('091584','16346','2','18'),('091584','16346','2','19'),('091584','16346','2','20'),('091584','16346','2','21'),('091584','16346','2','22');
/*!40000 ALTER TABLE `waiting_list` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger waiting_insert after insert on waiting_list for each row begin  declare booking_exists int; set booking_exists = (select count(booking_number) from waiting where booking_number=NEW.booking_number); if booking_exists = 0 then insert into waiting values (NEW.booking_number,NEW.doj,NEW.train_number); end if;  end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger waiting_update after update on waiting_list for each row begin  if NEW.waiting_list_number = 0 then delete from waiting where booking_number=NEW.booking_number; end if; end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger waiting_delete after delete on waiting_list for each row begin delete from waiting where booking_number = OLD.booking_number; end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-25 23:22:14
